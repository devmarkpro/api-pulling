export default class TimerHelper {
  static myInstance = null;
  _intervals = {}
  _timeouts = {}
  /**
   * @returns {TimerHelper}
   */
  static getInstance () {
    if (TimerHelper.myInstance == null) {
      TimerHelper.myInstance = new TimerHelper()
    }

    return this.myInstance
  }

  interval = (fn, tick, ref) => {
    console.log(ref.toString())
    const i = setInterval(fn, tick)
    if (!this._intervals[ref.toString()]) {
      this._intervals[ref.toString()] = []
    }
    this._intervals[ref.toString()].push(i)
    console.log('Adding interval ' + i + ' to ' + ref.toString())
    return i
  }
  timeout = (fn, tick, ref) => {
    console.log(ref.toString())
    const i = setInterval(fn, tick)
    if (!this._timeouts[ref.toString()]) {
      this._timeouts[ref.toString()] = []
    }
    this._timeouts[ref.toString()].push(i)
    console.log('Adding timeout ' + i + ' to ' + ref.toString())
    return i
  }

  clear = (index, ref) => {
    const intervals = this._intervals[ref.toString()]
    if (intervals) {
      intervals.forEach(i => {
        if (i === index) {
          clearInterval(index)
        }
      })
    }
    const timeouts = this._timeouts[ref.toString()]
    if (timeouts) {
      timeouts.forEach(i => {
        if (i === index) {
          clearTimeout(index)
        }
      })
    }
    return this
  }
  clearAll = (ref) => {
    if (ref) {
      if (this._intervals[ref.toString()]) {
        this._intervals[ref.toString()].forEach(i => clearInterval(i))
        delete this._intervals[ref.toString()]
      }
      if (this._timeouts[ref.toString()]) {
        this._timeouts[ref.toString()].forEach(i => clearTimeout(i))
        delete this._timeouts[ref.toString()]
      }
    } else {
      Object.keys(this._intervals).forEach(key => {
        this._intervals[key].forEach(m => clearInterval(m))
      })
      Object.keys(this._timeouts).forEach(key => {
        this._timeouts[key].forEach(m => clearInterval(m))
      })
      this._timeouts = []
    }
    return this
  }
}
