import TimerHelper from './Timer'
import axios from 'axios'
// flow
export default class APIPulling {
  static myInstance = null;
  static PULLERS = {
    HOME: 'PULLING.HOME',
    CONTEST: 'PULLING.CONTEST'
  }

  _STATES = {}
  _INITIAL_STATE = {
    ID: undefined,
    DEFINED: false,
    START: null,
    RUNNER: 0,
    MAX: 0,
    EXPIRE: null,
    INTERVAL: null,
    ENDPOINT: null,
    DATA: null,
    CALLBACK: () => {},
    ERRORS: []
  }
  constructor () {
    Object.keys(APIPulling.PULLERS).forEach(puller => { this._STATES[puller] = this._INITIAL_STATE })
    this._timer = TimerHelper.getInstance()
  }
  /**
   * @returns {APIPulling}
   */
  static getInstance (): APIPulling {
    if (APIPulling.myInstance == null) {
      APIPulling.myInstance = new APIPulling()
    }

    return this.myInstance
  }

  start = ({ type, meta, method = 'GET', header = null }) => {
    this._validateType(type)
    const newState = Object.assign(this._INITIAL_STATE, meta)
    const uniqueType = this._createUniqueType(type, newState.ENDPOINT)
    newState.DEFINED = true
    newState.ERRORS = []
    newState.ID = this._timer.interval(() => {
      const ax = {
        method: method.toLowerCase(),
        url: newState.ENDPOINT,
        data: newState.DATA
      }
      if (header) {
        ax.header = header
      }
      axios(ax).then((response) => {
        newState.CALLBACK(response)
        this._timer.clear(newState.ID, uniqueType)
      }).catch((error) => {
        const count = newState.RUNNER + 1
        if (count >= newState.MAX) {
          this._timer.clear(newState.ID, uniqueType)
        } else if (newState.EXPIRE && new Date() >= new Date(newState.DATE)) {
          this._timer.clear(newState.ID, uniqueType)
        }
        newState.ERRORS.push(error)
      })
    }, newState.INTERVAL, uniqueType)
    return true
  }
  forceStop = ({ type, endpoint }) => {
    try {
      this._validateType(type)
      try {
        this._timer.clearAll(this._createUniqueType(type, endpoint))
        return true
      } catch (error) {
        return false
      }
    } catch {
      return true
    }
  }
  forceStopAll = ({ type }) => {
    try {
      this._validateType(type)
      try {
        const keys = []
        Object.keys(this._timer._intervals).forEach(key => {
          if (key.startsWith(type)) {
            keys.push(this._timer._intervals[key])
          }
        })
        keys.forEach(k => { this._timer.clear(k, type) })
        return true
      } catch (error) {
        return false
      }
    } catch {
      return true
    }
  }
  _createUniqueType = (type, endpoint) => type + '_' + endpoint
  _validateType = type => {
    if (!this._STATES[type]) {
      throw APIPullingError(`type should be one of ${APIPulling.PULLERS.map(a => (a + ','))}`)
    }
    const {
      DEFINED
    } = this._STATES[type]
    if (DEFINED === true) {
      throw APIPullingError(`type '${type}', already defined`)
    }
  }
}
class APIPullingError extends Error {
  constructor (message) {
    const msg = 'APIPulling: ' + message
    super(msg)
  }
}
